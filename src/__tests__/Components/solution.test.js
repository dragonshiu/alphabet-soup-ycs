import { render, screen } from '@testing-library/react';
import Solution from '../../Components/solution';

describe('Solution', () => {
    test('renders Solution, hidden list', () => {
        render(<Solution shown={false} />);
        const btn = screen.getByTestId('sln-btn');
        const list = screen.queryByTestId('solution-list');
        expect(btn).toBeInTheDocument();
        expect(list).toBeNull();
    });
});
