import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import CustomInput from '../../Components/customInput';

describe('CustomInput', () => {
    test('renders CustomInput', () => {
        render(<CustomInput />);
        const msg = screen.getByTestId('input-message');
        expect(msg).toBeInTheDocument();
    });

    test('upload function', async () => {
        const file = new File(['foo'], 'foo.txt', {
            type: 'text/plain',
        });
        render(<CustomInput />);
        const input = screen.getByTestId('file-btn');
        userEvent.upload(input, file);

        expect(input.files[0]).toStrictEqual(file);
        expect(input.files).toHaveLength(1);
    });
});
