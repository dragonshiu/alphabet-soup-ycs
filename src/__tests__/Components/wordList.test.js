import { render, screen } from '@testing-library/react';
import WordList from '../../Components/wordList';

describe('WordList', () => {
    test('renders WordList with no empty words', () => {
        render(<WordList words={[]} />);
        const lists = screen.queryAllByRole('li');
        expect(lists.length).toBe(0);
    });

    test('renders WordList with populated words', () => {
        render(<WordList words={['hello', 'world']} />);
        const lists = screen.queryAllByRole('li');
        expect(lists.length).toBe(2);
    });
});
