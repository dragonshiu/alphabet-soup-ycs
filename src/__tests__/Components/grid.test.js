import { render, screen } from '@testing-library/react';
import Grid from '../../Components/grid';

describe('Grid', () => {
    test('renders Grid with empty chars, should return null', () => {
        const { container } = render(<Grid chars={[]} />);
        expect(container).toBeEmptyDOMElement();
    });

    test('renders Grid with populated chars, should return 2 rows', () => {
        render(
            <Grid
                chars={[
                    ['a', 'b'],
                    ['c', 'd'],
                ]}
            />
        );
        const gridRow0 = screen.getByTestId('grid-row0');
        const gridRow1 = screen.getByTestId('grid-row1');
        expect(gridRow0).toBeInTheDocument();
        expect(gridRow1).toBeInTheDocument();
    });
});
