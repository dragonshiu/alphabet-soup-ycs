import { render, screen } from '@testing-library/react';
import App from '../App';

describe('App', () => {
    test('renders App', () => {
        render(<App />);
        const outer = screen.getByTestId('outside-wrapper');
        const inner = screen.getByTestId('inner-wrapper');
        expect(outer).toBeInTheDocument();
        expect(inner).toBeInTheDocument();
    });
});
