import { Trie } from '../../Utils/trie';

describe('Trie class', () => {
    const trie = new Trie();

    const insertSpy = jest.spyOn(trie, 'insert');
    const removeSpy = jest.spyOn(trie, 'remove');

    describe('insert', () => {
        test('defines insert', () => {
            expect(typeof trie.insert).toBe('function');
        });

        test('invocation', () => {
            expect(trie.insert('hello')).toBeUndefined();
            expect(insertSpy).toHaveBeenCalled();
        });
    });

    describe('remove', () => {
        test('defines remove', () => {
            expect(typeof trie.remove).toBe('function');
        });

        test('invocation', () => {
            expect(trie.remove('hello')).toBeUndefined();
            expect(removeSpy).toHaveBeenCalled();
        });
    });
});
