import { findWords } from '../../Utils/logic';
import { getChars } from '../../Utils/utils';

const chars = getChars(['3x3', 'a b c', 'd e f', 'g h i'], 3);

const words = ['abc', 'gec'];

const rows = 3,
    cols = 3;

describe('Logic', () => {
    test('findWords, good case', () => {
        const res = findWords(chars, words, rows, cols);
        expect(res).toStrictEqual({
            abc: {
                start: [0, 0],
                body: [
                    [0, 1],
                    [0, 2],
                ],
                end: [0, 2],
            },
            gec: {
                start: [2, 0],
                body: [
                    [1, 1],
                    [0, 2],
                ],
                end: [0, 2],
            },
        });
    });

    test('findWords, empty chars', () => {
        const res = findWords([], words, rows, cols);
        expect(res).toStrictEqual({});
    });

    test('findWords, empty words', () => {
        const res = findWords(chars, [], rows, cols);
        expect(res).toStrictEqual({});
    });

    test('findWords, missing rows', () => {
        const res = findWords(chars, [], 0, cols);
        expect(res).toStrictEqual({});
    });

    test('findWords, missing cols', () => {
        const res = findWords(chars, [], rows, 0);
        expect(res).toStrictEqual({});
    });

    test('findWords, weird rows', () => {
        const res = findWords(chars, [], 1, cols);
        expect(res).toStrictEqual({});
    });

    test('findWords, weird cols', () => {
        const res = findWords(chars, [], rows, 1);
        expect(res).toStrictEqual({});
    });
});
