import { getWords, getChars } from '../../Utils/utils';

describe('getWords', () => {
    test('invoked with no args', () => {
        const res = getWords();
        expect(typeof res).toBe('object');
        expect(res.length).toBe(0);
    });

    test('invoked with only textArr args', () => {
        const res = getWords(['hello', 'world']);
        expect(typeof res).toBe('object');
        expect(res.length).toBe(1);
        expect(res[0]).toBe('world');
    });

    test('invoked with only rows args', () => {
        const res = getWords([], 5);
        expect(typeof res).toBe('object');
        expect(res.length).toBe(0);
    });

    test('invoked with args', () => {
        const res = getWords(['2x2', 'a b', 'c d', 'ad', 'cd'], 2);
        expect(typeof res).toBe('object');
        expect(res.length).toBe(2);
        expect(res[0]).toBe('ad');
        expect(res[1]).toBe('cd');
    });
});

describe('getChars', () => {
    test('invoked with no args', () => {
        const res = getChars();
        expect(typeof res).toBe('object');
        expect(res.length).toBe(0);
    });

    test('invoked with only textArr args', () => {
        const res = getChars(['2x2', 'a b', 'c d', 'ad', 'cd']);
        expect(typeof res).toBe('object');
        expect(res.length).toBe(0);
    });

    test('invoked with only rows args', () => {
        const res = getChars([], 5);
        expect(typeof res).toBe('object');
        expect(res.length).toBe(0);
    });

    test('invoked with args', () => {
        const res = getChars(['2x2', 'a b', 'c d', 'ad', 'cd'], 2);
        expect(typeof res).toBe('object');
        expect(res.length).toBe(2);
        expect(res[0][0]).toStrictEqual({ character: 'a', status: false });
        expect(res[0][1]).toStrictEqual({ character: 'b', status: false });
        expect(res[1][0]).toStrictEqual({ character: 'c', status: false });
        expect(res[1][1]).toStrictEqual({ character: 'd', status: false });
    });
});
