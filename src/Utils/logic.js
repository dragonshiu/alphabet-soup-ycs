import { Trie } from './trie';

//can be a basic array. i built it think i need to track the direction of start/end
const directions = {
    top: [-1, 0],
    tr: [-1, 1],
    right: [0, 1],
    br: [1, 1],
    bot: [1, 0],
    bl: [1, -1],
    left: [0, -1],
    tl: [-1, -1],
};

//top,  tl, tr, l are all reverse

export function findWords(chars, words, rows = 0, cols = 0) {
    if (!chars.length) return {};

    if (chars.length !== rows || chars[0].length !== cols) return {};

    let start = null;
    let body = [];
    const results = {};
    const trie = new Trie();

    for (const word of words) {
        trie.insert(word.replace(/[^a-z0-9]/gi, '').toLowerCase());
    }

    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            search(row, col, trie.root);
        }
    }

    // return trie;

    function search(row, col, node, directionKey = null) {
        let indMatch = 0;

        let dRow, dCol;
        if (directionKey) {
            [dRow, dCol] = directions[directionKey];
        }

        // let reverse = ['top', 'tl', 'tr', 'left', 'bl']?.includes(directionKey);

        // matched
        if (node.word) {
            // if one of the reverse direction, swap start and end value
            //this is no longer needed, i misread the ac
            results[node.word] = { start, end: [row - dRow, col - dCol], body };
            // results[node.word] = reverse
            // ? { start: [row - dRow, col - dCol], end: start }
            // : { start, end: [row - dRow, col - dCol] };
            trie.remove(node.word);
            indMatch++;
        }

        // out of bound
        if (row < 0 || row >= rows || col < 0 || col >= cols) {
            start = null;
            body = [];
            return indMatch;
        }

        const char = chars[row][col].character;

        // not in trie
        if (!node[char.toLowerCase()]) {
            start = null;
            body = [];
            return indMatch;
        }

        if (!start && directionKey) {
            start = [row - dRow, col - dCol];
        }

        // part of trie, not IT tho
        chars[row][col].character = '#';
        body.push([row, col]);

        // record max number of words we can find with current prefix
        const maxCount = node[char.toLowerCase()].prefixCount;

        let totalMatch = 0;

        const newObj = {};
        if (directionKey) {
            newObj[directionKey] = directions[directionKey];
        }
        const iterator = directionKey ? Object.entries(newObj) : Object.entries(directions);

        for (const [key, value] of iterator) {
            let [dRow, dCol] = value;
            totalMatch += search(row + dRow, col + dCol, node[char.toLowerCase()], key);

            // subtree cleared
            if (totalMatch === maxCount) break;
        }

        chars[row][col].character = char;
        return indMatch + totalMatch;
    }

    return results;
}
