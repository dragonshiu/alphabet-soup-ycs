export class Trie {
    constructor() {
        this.root = {};
    }

    insert(word) {
        let trieNode = this.root;

        for (const char of word) {
            if (!trieNode[char]) {
                trieNode[char] = {};
            }

            trieNode = trieNode[char];
            // this prefix tracks how many children we have left
            // if prefix is 0, we can delete the whole subtree
            trieNode.prefixCount = (trieNode.prefixCount || 0) + 1;
        }

        //this is the endOfWord flag, but keeping word here so i can use it in the logic
        trieNode.word = word;
    }

    remove(word) {
        let trieNode = this.root;

        for (const char of word) {
            trieNode[char].prefixCount--;

            if (trieNode[char].prefixCount === 0) {
                delete trieNode[char];
                return;
            }
            trieNode = trieNode[char];
        }

        delete trieNode.word;
    }
}
