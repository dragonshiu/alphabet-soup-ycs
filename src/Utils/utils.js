export function getWords(textArr = [], rows = 0) {
    const listArr = [];
    //account for ln1 and all the rows
    const wordListIndex = rows + 1;

    for (let i = wordListIndex; i < textArr.length; i++) {
        listArr.push(textArr[i]);
    }
    return listArr;
}

export function getChars(textArr = [], rows = 0) {
    const out = [];

    // edge case caught by test
    if (rows >= textArr.length) {
        return out;
    }

    const wordListIndex = rows + 1;
    //skip ln1
    for (let i = 1; i < wordListIndex; i++) {
        out.push(
            textArr[i].split(' ').map((c) => {
                return { character: c, status: false };
            })
        );
    }
    return out;
}
