import { useState, useEffect, useMemo, useCallback } from 'react';
import { getWords, getChars } from './Utils/utils';

import text from './texts/test.txt'; //here is where you would import a different text file to test
import './App.css';
import Grid from './Components/grid';
import Solution from './Components/solution';
import WordList from './Components/wordList';
import { findWords } from './Utils/logic';
import CustomInput from './Components/customInput';

function App() {
    const [file, setFile] = useState(null);
    const [rows, setRows] = useState(null);
    const [cols, setCols] = useState(null);
    const [textArr, setTextArr] = useState([]);
    const [chars, setChars] = useState([]);
    const [shown, setShown] = useState(false);

    //make every char to cap to skip sensitive check
    //recreate word list here with space and without space
    //utils
    const words = useMemo(() => {
        return getWords(textArr, rows);
    }, [rows, textArr]);

    const getText = async () => {
        const res = await fetch(text); //review comment on ln4
        const out = await res.text();
        return out;
    };

    //breaking when not saving a file
    const extractInfo = async () => {
        let text;
        if (!file) {
            text = await getText();
        } else {
            text = file;
        }
        const textArr = text.split('\n').map((line) => line.trim());
        let [row, col] = textArr[0].split('x');

        setRows(+row);
        setCols(+col);
        setTextArr(textArr);
        setShown(false);
        setChars(getChars(textArr, +row));
    };

    useEffect(() => {
        extractInfo();
    }, [file]);

    const getSolution = useCallback(() => findWords(chars, words, rows, cols), [chars, words, rows, cols]);
    return (
        <div className="outter" data-testid="outside-wrapper">
            <CustomInput setFile={setFile} />
            <div className="inner" data-testid="inner-wrapper">
                <div>
                    <Grid chars={chars} shown={shown} />
                    <Solution
                        getSolution={getSolution}
                        setChars={setChars}
                        words={words}
                        chars={chars}
                        setShown={setShown}
                        shown={shown}
                    />
                </div>
                <WordList words={words} />
            </div>
        </div>
    );
}

export default App;
