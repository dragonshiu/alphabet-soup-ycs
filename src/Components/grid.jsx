import { styled } from 'styled-components';

const Grid = ({ chars, shown }) => {
    if (!chars.length) {
        return null;
    }
    return (
        <div>
            <Board>
                {chars.map((rows, i) => {
                    return (
                        <div className="row" data-testid={`grid-row${i}`} key={i}>
                            {rows.map((c, ix) => {
                                const { character, status } = c;
                                return (
                                    <Tile key={ix} className={shown && status ? 'mark' : ''}>
                                        {character}
                                    </Tile>
                                );
                            })}
                        </div>
                    );
                })}
            </Board>
        </div>
    );
};

export default Grid;

const Tile = styled.div`
    border: 1px black solid;
    margin: 1px;
    width: 50px;
    height: 50px;
    &.mark {
        background-color: yellow;
    }
`;

const Board = styled.div`
    .row {
        display: flex;
        flex-direction: row;
    }
`;
