import React, { useState } from 'react';

const Solution = ({ getSolution, setChars, words, chars, shown, setShown }) => {
    const [display, setDisplay] = useState(null);

    const handleClick = () => {
        const sol = getSolution();
        setDisplay(sol);
        setShown((p) => !p);
        colorUp(sol);
    };

    const colorUp = (sol) => {
        let array = [];
        words.forEach((w, i) => {
            const mapW = w.replace(/[^a-z]/gi, '').toLowerCase();
            if (sol[mapW]) {
                const { start, body } = sol[mapW];
                //this account for weird edge case where word is a single character
                if (start) array = [...array, start];
                if (body) array = [...array, ...body];
            }
        });
        let charsClone = chars;
        array.forEach((mark) => {
            charsClone[mark[0]][mark[1]].status = true;
        });
        setChars(charsClone);
    };

    const solutionDisplay = () => {
        return words.map((w, i) => {
            const mapW = w.replace(/[^a-z]/gi, '').toLowerCase();
            if (display[mapW]) {
                const { start, end } = display[mapW];
                //edge case where singular character is marked (then start would be null since we only populate when direction is 'set)
                if (!start) {
                    return <p key={i}>{`${w} ${end[0]}:${end[1]} ${end[0]}:${end[1]}`} </p>;
                }
                return <p key={i}>{`${w} ${start[0]}:${start[1]} ${end[0]}:${end[1]}`} </p>;
            }
            return <p key={i}>{w} Not found</p>;
        });
    };
    return (
        <>
            <p>Click the button to view solution</p>
            <button data-testid="sln-btn" onClick={handleClick}>
                View Solution
            </button>
            {shown && <div data-testid="solution-list">{solutionDisplay()}</div>}
        </>
    );
};

export default Solution;
