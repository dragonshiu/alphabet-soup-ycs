import React from 'react';

const CustomInput = ({ setFile }) => {
    function handleFile(event) {
        const reader = new FileReader();
        const file = event.target.files[0];
        if (file) {
            reader.readAsText(event.target.files[0]);
            reader.onload = () => {
                setFile(reader.result);
            };
            reader.onerror = () => {
                console.log('help');
            };
        }
    }

    return (
        <div>
            <p data-testid="input-message">You can manually enter .txt files in here to test</p>
            <p>ps: based on ac, txt file should be in correct format, so I did not account for incorrect input</p>
            <input data-testid="file-btn" type="file" name="inputfile" id="inputfile" onChange={handleFile} />
            <br />
        </div>
    );
};

export default CustomInput;
