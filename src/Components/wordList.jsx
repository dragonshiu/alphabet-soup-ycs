import { styled } from 'styled-components';

const WordList = ({ words }) => {
    return (
        <Wrapper>
            <h3> Word list:</h3>
            <ul>
                {words.map((l, i) => {
                    return (
                        <li role="li" key={i}>
                            {l}
                        </li>
                    );
                })}
            </ul>
        </Wrapper>
    );
};

export default WordList;

const Wrapper = styled.div`
    margin: 0 2em;
    ul {
        list-style: auto;
        padding-left: 1.5em;
    }
`;
